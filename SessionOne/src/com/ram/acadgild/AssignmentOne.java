package com.ram.acadgild;

public class AssignmentOne {
	
	public static void main(String[] args) {
		
		System.out.println(sumNum(5,5.7f));
		
	}
	
	private static int sumNum(int x, float y) {
		return x + (int)y;
	}

}
